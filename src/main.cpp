#include <iostream>
using namespace std;

void NumberInput() {
    int UsrNum; // this makes a variable
    cout << "Type your favorite number will ya: "; 
    cin >> UsrNum; // this gets checks for user input and loads it into UsrNum
     //if the number you typed is 69 then it displays Nice in the command line if not then it says it hates that number 
    if (UsrNum==69){ 
            cout << "Nice \n";
     } else {
      cout << "Ew I hate the number " << UsrNum << endl; // this displays the number in UsrNum and then to go down a line
   }
}

void PrgEnd() {
    string Name; // this makes a string called name
    cout << "To exit the program please type your name: ";
    cin >> Name; // this gets what you typed
    cout << "Thanks for playing: " << Name << endl;// this displays what you type
}

int main() {
  NumberInput(); // this runs the NumberInput function
  PrgEnd(); // this runs the PrgEnd function
  return 0;// this tells all the functions and code to go back to main aka the start
}

